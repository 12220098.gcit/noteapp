package Model

import (
	"myapp/dataStore/postgres"
	"time"
)

type Note struct {
	NoteId    int       `json:"id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

const queryInsertNote = "INSERT INTO note ( title, content, created_at, updated_at) VALUES ($1, $2, $3, $4);"

func (n *Note) Create() error {
	n.CreatedAt = time.Now()
	n.UpdatedAt = time.Now()

	_, err := postgres.Db.Exec(queryInsertNote, n.Title, n.Content, n.CreatedAt, n.UpdatedAt)
	if err != nil {
		return err
	}

	return nil
}

func GetNoteByID(noteID int) (*Note, error) {
	var note Note

	const queryGetNote = "SELECT * FROM note WHERE id = $1"

	err := postgres.Db.QueryRow(queryGetNote, noteID).Scan(&note.NoteId, &note.Title, &note.Content, &note.CreatedAt, &note.UpdatedAt)
	if err != nil {
		return nil, err
	}

	return &note, nil
}

const queryUpdateNote = "UPDATE note SET title = $1, content = $2, updated_at = $3 WHERE id = $4"

func UpdateNoteByID(noteID int, note *Note) error {
	_, err := postgres.Db.Exec(queryUpdateNote, note.Title, note.Content, time.Now(), noteID)
	if err != nil {
		return err
	}

	return nil
}

const queryDeleteNote = "DELETE FROM note WHERE id = $1"

func DeleteNoteByID(noteID int) error {

	_, err := postgres.Db.Exec(queryDeleteNote, noteID)
	if err != nil {
		return err
	}

	return nil
}

func GetAllNotes() ([]Note, error) {
	rows, getErr := postgres.Db.Query("SELECT * FROM note;")
	if getErr != nil {
		return nil, getErr
	}
	notes := []Note{}

	for rows.Next() {
		var n Note
		dbErr := rows.Scan(&n.NoteId, &n.Title, &n.Content, &n.CreatedAt, &n.UpdatedAt)
		if dbErr != nil {
			return nil, dbErr
		}
		notes = append(notes, n)
	}
	rows.Close()
	return notes, nil
}
