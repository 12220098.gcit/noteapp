package Model

import "myapp/dataStore/postgres"

// signup
type SignUp struct {
	Name     string
	Email    string
	Password string
}

const queryInsertAdmin = "INSERT INTO admin(Name, Email, Password) VALUES ($1, $2, $3);"

func (adm *SignUp) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.Name, adm.Email, adm.Password)
	return err
}

// login
type Login struct {
	Email    string
	Password string
}

const queryGetAdmin = "SELECT email, password FROM admin WHERE email = $1 and password = $2;"

func (adm *Login) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}
