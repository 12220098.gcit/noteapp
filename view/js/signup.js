function signUp() {
  var adminData = { 
    name: document.getElementById("username").value,
    email: document.getElementById("email").value,
    password: document.getElementById("pw1").value,
    pw: document.getElementById("pw2").value,
  };

  if (adminData.password !== adminData.pw) {
    alert("Passwords do not match!");
    return;
  }
  fetch('/signup', {
    method: "POST",
    body: JSON.stringify(adminData),
    headers: { "content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => {
      if (response.ok) {
        console.log("Redirecting to the login page");
        window.open("index.html", "_self");
      } else {
        throw new Error("Network response was not ok.");
      }
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}
