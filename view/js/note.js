const addBox = document.querySelector(".add-box");
popupBox = document.querySelector(".popup-box");
popupTitle = popupBox.querySelector("header p");
closeIcon = popupBox.querySelector("header i");
titleTag = popupBox.querySelector("input");
descTag = popupBox.querySelector("textarea");
addBtn = popupBox.querySelector("button");

const months = [
  "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
];

  let notes = [];
  let currentDate = new Date();
  let month = months[currentDate.getMonth()];
  let day = currentDate.getDate();
  let year = currentDate.getFullYear();
  let noteInfo = { title, content: description, date:`${month} ${day}, ${year}`};
  
function getFormData(){
  var formData = {
    title : document.getElementById('title').value,
    description : document.getElementById('description').value,
    date:`${month} ${day}, ${year}`
    }
  return formData
}

// Fetch data from the backend
// fetch('/note')
//   .then(response => response.json())
//   .then(data => {
//     // Store the retrieved notes in the notes array
//     data = getFormData()
//     // Call the showNotes function to display the notes on the page
//     showNotes(data);
//   })
//   .catch(error => {
//     console.error('Error fetching notes:', error);
//   });

let isUpdate = false;
let updateId;

addBox.addEventListener("click", () => {
  popupTitle.innerText = "Add a new Note";
  addBtn.innerText = "Add Note";
  popupBox.classList.add("show");
  document.querySelector("body").style.overflow = "hidden";
  if (window.innerWidth > 660) titleTag.focus();

});

closeIcon.addEventListener("click", () => {
  isUpdate = false;
  titleTag.value = descTag.value = "";
  popupBox.classList.remove("show");
  document.querySelector("body").style.overflow = "auto";
});

// function showNotes(note) {
//   alert(note.title)
//     if (!notes) return;
//     document.querySelectorAll(".note").forEach(li => {
//       console.log("LI TEST",li); 
//       li.remove()});
//     notes.forEach((note, id) => {
//       console.log("NOTE STATEMENT" ,note); // Debugging statement
//       console.log("ID", id)
//       let filterDesc = note.description ? note.description.replaceAll("\n", '<br/>') : "";
//       let liTag = `<li class="note">
//                     <div class="details">
//                         <p>`+note.title+`</p>
//                         <span>${filterDesc}</span>
//                     </div>
//                     <div class="bottom-content">
//                         <span id="time">${note.date}</span>
//                         <div class="settings">
//                             <i onclick="showMenu(this)" class="uil uil-ellipsis-h"></i>
//                             <ul class="menu">
//                                 <li onclick="updateNote(${id}, '${note.title}', '${filterDesc}')"><i class="uil uil-pen"></i>Edit</li>
//                                 <li onclick="deleteNote(${id})" id='id'><i class="uil uil-trash"></i>Delete</li>
//                             </ul>
//                         </div>
//                     </div>
//                 </li>`;
//       addBox.insertAdjacentHTML("afterend", liTag);
//     });
//   }

function showNotes(notes) {
  if (!notes) return;

  document.querySelectorAll(".note").forEach(li => {
    console.log("LI TEST", li); 
    li.remove();
  });

  notes.forEach((note, id) => {
    console.log("NOTE STATEMENT", note); // Debugging statement
    console.log("ID", id);

    let filterDesc = note.description ? note.description.replaceAll("\n", '<br/>') : "";
    let liTag = `<li class="note">
      <div class="details">
        <p>${note.title}</p>
        <span>${filterDesc}</span>
      </div>
      <div class="bottom-content">
        <span id="time">${note.date}</span>
        <div class="settings">
          <i onclick="showMenu(this)" class="uil uil-ellipsis-h"></i>
          <ul class="menu">
            <li onclick="updateNote(${id}, '${note.title}', '${filterDesc}')"><i class="uil uil-pen"></i>Edit</li>
            <li onclick="deleteNote(${id})" id='${id}'><i class="uil uil-trash"></i>Delete</li>
          </ul>
        </div>
      </div>
    </li>`;

    addBox.insertAdjacentHTML("afterend", liTag);
  });
}


function showMenu(elem) {
  elem.parentElement.classList.add("show");
  document.addEventListener("click", e => {
    if (e.target.tagName != "I" || e.target != elem) {
      elem.parentElement.classList.remove("show");
    }
  });
}
function showAllNotes(data) {
  const Notes = JSON.parse(data)
  Notes.forEach(not => {
      showNotes(not);
    });
}

function deleteNote(noteId) {
  console.log("FUNCTION TEST")
  let confirmDel = confirm("Are you sure you want to delete this note?");
  if (!confirmDel) return;
  notes.splice(noteId, 1);
  // Make a DELETE request to the backend to remove the note from the database
  fetch(`/note/${id}`, { 
    method: 'DELETE',
    headers:{"content-type": "application/json; charset=UFT-8"} })
    .then(response => {
      console.log("RESPONSE TEST", response)
      console.log("STATUS RESPONSE", response.ok)
      if (response.ok) {
        console.log("FLOW test")
        // Note successfully deleted from the database
        // showNotes();
      } else {
        throw new Error('Failed to delete note');
      }
    })
    .catch(error => {
      console.error('Error deleting note:', error);
    });

    let notes = [];
}

function updateNote(noteId, title, filterDesc) {
  let description = filterDesc.replaceAll('<br/>', '\r\n');
  updateId = noteId;
  isUpdate = true;
  addBox.click();
  titleTag.value = title;
  descTag.value = description;
  popupTitle.innerText = "Update a Note";
  addBtn.innerText = "Update Note";
}

addBtn.addEventListener("click", e => {
  e.preventDefault();
  let title = titleTag.value.trim();
  let description = descTag.value.trim();

  if (title || description) {
    let currentDate = new Date();
    let month = months[currentDate.getMonth()];
    let day = currentDate.getDate();
    let year = currentDate.getFullYear();

    let noteInfo = { title, content: description, date:`${month} ${day}, ${year}` };
    console.log("noteInfo:", noteInfo)
    if (!isUpdate) {
      // Make a POST request to the backend to add the note to the database
      fetch('/note', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(noteInfo)
      })
        .then(response => {
          if (response.ok) {
            // Note successfully added to the database
            return response.json();
          } else {
            throw new Error('Failed to add note');
          }
        })
        .then(data => {
          // data = getFormData()
          notes.push(noteInfo);
showNotes(notes);

          console.log("TEST DATA", data)
          // data['id'] = 
          // Update the notes array with the newly added note from the database
          notes.push(data);
          showNotes();
          closeIcon.click();
        })
        .catch(error => {
          console.error('Error adding note:', error);
        });
    } else {
      // Make a PUT request to the backend to update the note in the database
      fetch(`/note/${updateId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(noteInfo)
      })
        .then(response => {
          if (response.ok) {
            // Note successfully updated in the database
            showNotes();
            closeIcon.click();
          } else {
            throw new Error('Failed to update note');
          }
        })
        .catch(error => {
          console.error('Error updating note:', error);
        });
    }
  }
});

window.onload = function() {
  fetch('/notes', {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then(data => showAllNotes(JSON.stringify(data)));
  
}  