package Controller

import (
	"encoding/json"
	"fmt"
	"myapp/Model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Addnote(w http.ResponseWriter, r *http.Request) {
	var note Model.Note

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&note); err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Invalid Json Type"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	saveErr := note.Create()
	fmt.Println(saveErr)
	if saveErr != nil {
		// w.Write([]byte("Database error"))
		response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}
	//no error
	response, _ := json.Marshal(map[string]string{"status": "note added"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func GetNote(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	noteIDStr := params["id"]

	noteID, err := strconv.Atoi(noteIDStr)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Invalid note ID"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	note, err := Model.GetNoteByID(noteID)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Note not found"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		w.Write(response)
		return
	}

	response, _ := json.Marshal(note)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func UpdateNote(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	noteIDStr := params["id"]

	noteID, err := strconv.Atoi(noteIDStr)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Invalid note ID"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	var note Model.Note

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&note); err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Invalid JSON payload"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	err = Model.UpdateNoteByID(noteID, &note)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Failed to update note"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(response)
		return
	}

	response, _ := json.Marshal(map[string]string{"status": "Note updated"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Controller to get the last added id from the database


// func DeleteNote(w http.ResponseWriter, r *http.Request) {
// 	params := mux.Vars(r)
// 	noteIDStr := params["id"]

// 	noteID, err := strconv.Atoi(noteIDStr)
// 	if err != nil {
// 		response, _ := json.Marshal(map[string]string{"error": "Invalid note ID"})
// 		w.Header().Set("Content-Type", "application/json")
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write(response)
// 		return
// 	}

// 	err = Model.DeleteNoteByID(noteID)
// 	if err != nil {
// 		response, _ := json.Marshal(map[string]string{"error": "Failed to delete note"})
// 		w.Header().Set("Content-Type", "application/json")
// 		w.WriteHeader(http.StatusInternalServerError)
// 		w.Write(response)
// 		return
// 	}

// 	response, _ := json.Marshal(map[string]string{"status": "Note deleted"})
// 	w.Header().Set("Content-Type", "application/json")
// 	w.WriteHeader(http.StatusOK)
// 	w.Write(response)
// }

func DeleteNote(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	noteIDStr := params["id"]

	noteID, err := strconv.Atoi(noteIDStr)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Invalid note ID"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	err = Model.DeleteNoteByID(noteID)
	if err != nil {
		response, _ := json.Marshal(map[string]string{"error": "Failed to delete note"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(response)
		return
	}

	response, _ := json.Marshal(map[string]string{"status": "Note deleted"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetAllNotes(w http.ResponseWriter, r *http.Request) {
	notes, getErr := Model.GetAllNotes()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, notes)
}
