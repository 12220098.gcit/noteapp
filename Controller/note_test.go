package Controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddNote(t *testing.T) {
	url := "http://localhost:1313/note"

	var jsonStr = []byte(`{"id":100, "title": "EVS notes", "content": "This is EVS notes"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status":"note added"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestGetNote(t *testing.T) {
	c := http.Client{}
	r, _ := c.Get("http://localhost:1313/note/4")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"id":0,"title":"English notes", "content":"ABC", "created_at":"2023-06-09T15:50:14.529574Z","updated_at":"2023-06-09T15:50:14.529574Z"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteNote(t *testing.T) {
	url := "http://localhost:1313/note/3"

	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"Note deleted"}`
	assert.JSONEq(t, expResp, string(body))

}

func TestStudentNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:1313/note/3")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Note not found"}`
	assert.JSONEq(expResp, string(body))
}
