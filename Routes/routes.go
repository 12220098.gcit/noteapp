package Routes

import (
	"log"
	"myapp/Controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {

	router := mux.NewRouter()

	//signup
	router.HandleFunc("/signup", Controller.Signup).Methods("POST")

	//login
	router.HandleFunc("/login", Controller.Login).Methods("POST")

	//Notes
	router.HandleFunc("/note", Controller.Addnote).Methods("POST")
	router.HandleFunc("/note/{id}", Controller.GetNote).Methods("GET")
	router.HandleFunc("/note/{id}", Controller.UpdateNote).Methods("PUT")
	router.HandleFunc("/note/{id}", Controller.DeleteNote).Methods("DELETE")
	router.HandleFunc("/notes", Controller.GetAllNotes).Methods("GET")
	// router.HandleFunc("/notesLastId", Controller.GetLastId).Methods("GET")
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 1313...")
	err := http.ListenAndServe(":1313", router)
	if err != nil {
		return
	}
}
